package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.dao.EmployeeDao;
import com.example.model.Employee;
import com.example.model.EmployeeRoleResult;
import com.example.service.EmployeeService;

@Controller
@RequestMapping("Employee")
public class EmployeeController {
	@Autowired
	EmployeeDao employeeDao;
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getById(@PathVariable("id") int id) {
		Employee emp = employeeDao.getEmployeeById(id);
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Employee>> getAll() {
		List<Employee> listEmp = employeeDao.getAll();
		return new ResponseEntity<List<Employee>>(listEmp, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Employee> addEmp(@RequestBody Employee emp) {
		employeeDao.addEmployee(emp);
		Employee emp2 = employeeDao.getEmployeeById(employeeDao.lastestInput());
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Employee> updateEmp(@PathVariable("id") int id, @RequestBody Employee emp) {
		employeeDao.updateEmployee(emp, id);
		Employee emp2 = employeeDao.getEmployeeById(id);
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Employee> deleteEmp(@PathVariable("id") int id) {
		Employee emp2 = employeeDao.getEmployeeById(id);
		employeeDao.deleteEmployeeById(id);
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}

}
