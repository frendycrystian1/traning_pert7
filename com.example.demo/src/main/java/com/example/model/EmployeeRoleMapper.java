package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeRoleMapper implements RowMapper<EmployeeRoleResult> {

	@Override
	public EmployeeRoleResult mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeRoleResult EmployeeResult = new EmployeeRoleResult();
		EmployeeResult.setRoleId(rs.getInt("roleId"));
		EmployeeResult.setRoleName(rs.getString("roleName"));
		return EmployeeResult;
	}

}
