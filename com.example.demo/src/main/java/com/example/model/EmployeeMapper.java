package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeMapper implements RowMapper<Employee>{

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee EmployeeResult = new Employee();
		EmployeeResult.setId(rs.getInt("employeeid"));
		EmployeeResult.setRoleId(rs.getInt("employeeroleid"));
		EmployeeResult.setName(rs.getString("employeename"));
		EmployeeResult.setPhone(rs.getString("phone"));
		EmployeeResult.setAddress(rs.getString("address"));
		return EmployeeResult;
	}
	
}
