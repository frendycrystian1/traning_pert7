package com.example.service;

import java.util.List;

import com.example.model.Employee;
import com.example.model.EmployeeRoleResult;

public interface EmployeeService {
	List<Employee> getAll();
	
	Employee getEmployeeById(int id);

	void addEmployee(Employee employee);

	void updateEmployee(Employee employee, int id);

	void deleteEmployeeById(int id);

	int lastestInput();
	
	List<EmployeeRoleResult> getAll2();
	
	EmployeeRoleResult getEmployeeRoleById(int id);

	void addEmployeeRole(Employee employee);

	void updateEmployeeRole(Employee employee, int id);

	void deleteEmployeeRoleById(int id);

	int RoleInput();

	void updateEmployee1(Employee employee, int id);

	void addEmployee1(Employee employee);

	void addEmployeeRole1(EmployeeRoleResult employee);

	void updateEmployeeRole1(EmployeeRoleResult employee, int id);
}
